import com.bama.financemodule.Invoice
import com.bama.financemodule.InvoiceRow
import com.bama.financemodule.Product

import java.text.DateFormat
import java.text.SimpleDateFormat

class BootStrap {

    def init = { servletContext ->
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy")

        Product fietsVentiel = new Product("Ventiel voor je fiets", 10.99, 21.00, 1).save(flush: true)
        Product fietsVentielDopje = new Product("Dopje voor op je  fietsventiel", 10.99, 21.00, 1).save(flush: true)

        InvoiceRow row1 = new InvoiceRow(fietsVentiel, 2)
        InvoiceRow row2 = new InvoiceRow(fietsVentielDopje, 4)

        Invoice inv0001 =new Invoice("fa001", new Date()).save(flush: true)
        if (inv0001){
            inv0001.addToInvoiceRow(row1)
            inv0001.addToInvoiceRow(row2)
        }

    }
    def destroy = {
    }
}
