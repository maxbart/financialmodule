package com.bama.financemodule

class InvoiceRow {

    String description
    double price
    double vat
    int stock
    int amount

    static belongsTo = [invoice: Invoice]

    InvoiceRow(Product product, int amount) {
        this()
        this.description = product.description
        this.price = product.price
        this.vat = product.vat
        this.stock = product.stock
        this.amount = amount
    }

    static constraints = {
        description(blank: false)
    }
}
