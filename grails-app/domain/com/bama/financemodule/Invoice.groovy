package com.bama.financemodule

import grails.rest.Resource

@Resource(uri = '/invoice')
class Invoice {

    String invoiceNumber
    Date date

    //TODO: add user to the invoice!

    Invoice(String invoiceNumber, Date date) {
        this()
        this.invoiceNumber = invoiceNumber
        this.date = date
    }

    static hasMany = [invoiceRow: InvoiceRow]

    static constraints = {
        invoiceNumber(blank: false)
    }
}