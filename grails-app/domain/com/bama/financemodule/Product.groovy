package com.bama.financemodule

class Product {

    String description
    double price
    double vat
    int stock

    Product(String description, double price, double vat, int stock) {
        this()
        this.description = description
        this.price = price
        this.vat = vat
        this.stock = stock
    }

    static constraints = {
        description(blank: false)
    }
}
