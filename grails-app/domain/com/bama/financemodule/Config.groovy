package com.bama.financemodule

import grails.rest.Resource

@Resource(uri = '/config')
class Config implements Serializable {

    String label
    String title
    String value

    Config(String label, String title, String value) {
        this()
        this.label = label
        this.title = title
        this.value = value
    }

    static constraints = {
        label(blank: false, unique: true, required: true, nullable: false)
        title(blank: false, required: true, nullable: false)
        value(blank: false, required: true, nullable: false)
    }

    String toString() {
        return title + ": " + value
    }
}

