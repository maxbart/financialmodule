package com.bama.financemodule

import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class InvoiceRowController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond InvoiceRow.list(params), model:[invoiceRowCount: InvoiceRow.count()]
    }

    def show(InvoiceRow invoiceRow) {
        respond invoiceRow
    }

    @Transactional
    def save(InvoiceRow invoiceRow) {
        if (invoiceRow == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (invoiceRow.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond invoiceRow.errors, view:'create'
            return
        }

        invoiceRow.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'invoiceRow.label', default: 'InvoiceRow'), invoiceRow.id])
                redirect invoiceRow
            }
            '*' { respond invoiceRow, [status: CREATED] }
        }
    }

    @Transactional
    def update(InvoiceRow invoiceRow) {
        if (invoiceRow == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (invoiceRow.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond invoiceRow.errors, view:'edit'
            return
        }

        invoiceRow.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'invoiceRow.label', default: 'InvoiceRow'), invoiceRow.id])
                redirect invoiceRow
            }
            '*'{ respond invoiceRow, [status: OK] }
        }
    }

    @Transactional
    def delete(InvoiceRow invoiceRow) {

        if (invoiceRow == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        invoiceRow.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'invoiceRow.label', default: 'InvoiceRow'), invoiceRow.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoiceRow.label', default: 'InvoiceRow'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
